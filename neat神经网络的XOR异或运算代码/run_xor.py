import os
import neat
import visualize

# 2个输入，1个输出的异或实验
xor_inputs = [(0.0, 0.0), (0.0, 1.0), (1.0, 0.0), (1.0, 1.0)]
xor_outputs = [(0.0,), (1.0,), (1.0,), (0.0,)]


def eval_genomes(genomes, config):
    # 评估函数
    for genome_id, genome in genomes:  # 每一个个体
        genome.fitness = 4.0  # 适应度为4.0的评估，因为有4个结果全部猜对
        net = neat.nn.FeedForwardNetwork.create(genome, config)  # 生成一个网络
        for xi, xo in zip(xor_inputs, xor_outputs):  # 把它丢进去训练
            output = net.activate(xi)
            genome.fitness -= (output[0] - xo[0]) ** 2  # 训练完后得到一个fitness


def run(config_file):
    # 读取配置文件
    config = neat.Config(neat.DefaultGenome, neat.DefaultReproduction,
                         neat.DefaultSpeciesSet, neat.DefaultStagnation,
                         config_file)

    # 创建种群
    p = neat.Population(config)

    # 打印训练过程
    p.add_reporter(neat.StdOutReporter(True))
    stats = neat.StatisticsReporter()
    p.add_reporter(stats)
    p.add_reporter(neat.Checkpointer(50))

    # 迭代300次
    winner = p.run(eval_genomes, 300)

    # 显示最佳网络
    print('\nBest genome:\n{!s}'.format(winner))
    print('\nOutput:')
    winner_net = neat.nn.FeedForwardNetwork.create(winner, config)
    for xi, xo in zip(xor_inputs, xor_outputs):
        output = winner_net.activate(xi)
        print("input {!r}, expected output {!r}, got {!r}".format(xi, xo, output))

    # 打印网络结构
    node_names = {-1: 'A', -2: 'B', 0: 'A XOR B'}
    visualize.draw_net(config, winner, True, node_names=node_names)
    visualize.plot_stats(stats, ylog=False, view=True)
    visualize.plot_species(stats, view=True)

    p = neat.Checkpointer.restore_checkpoint('neat-checkpoint-49')
    p.run(eval_genomes, 10)


if __name__ == '__main__':
    local_dir = os.path.dirname(__file__)
    config_path = os.path.join(local_dir, 'config-feedforward')
    run(config_path)
